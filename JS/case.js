/**
 * @author Johnny Tsheke
 */

$(document).ready(function(){
	//dessiner une case
	var canvasCase = document.getElementById("canvascase");//javascript 
	//var canvasCase=$("#canvascase")[0];  //jquery
	var ctxtCase = canvasCase.getContext("2d"); 
	 ctxtCase.strokeText("(300,20)",300,20);
	 ctxtCase.strokeText("(150,490)",100,490);//coordonnée x ajustée pour lisibilité
	 ctxtCase.strokeText("(450,90)",450,90);
	 ctxtCase.strokeStyle = "blue";
	 ctxtCase.lineWidth="5";
	 ctxtCase.beginPath();
	 ctxtCase.moveTo(450,490);
	 ctxtCase.lineTo(450,90);
	 ctxtCase.lineTo(150,90);
	 ctxtCase.lineTo(300,20);
	 ctxtCase.lineTo(450,90);
	 ctxtCase.lineTo(150,490);
	 ctxtCase.lineTo(450,490);
	 ctxtCase.lineTo(150,90);
	 ctxtCase.lineTo(150,490);
	 ctxtCase.stroke();
	 
});